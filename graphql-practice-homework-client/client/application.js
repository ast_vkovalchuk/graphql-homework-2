import * as queries from './queries';

const SELECTORS = {
    FLIGHTS: '.js-flights-button',
    RESULTS: '.js-results-wrapper',
    REGISTER_TITLE: '.js-register-title',
    LOGIN_TITLE: '.js-login-title',
    USER_LOGIN: '.js-user-login',
    USER_PASSWORD: '.js-user-password',
    REGISTER: '.js-submit-register',
    LOGIN: '.js-submit-login'
};

export default class Application {
    constructor(client) {
        this.client = client;
        this.wrapper = document.querySelector(SELECTORS.RESULTS);
        
        this.buttons = {
            register: document.querySelector(SELECTORS.REGISTER),
            login: document.querySelector(SELECTORS.LOGIN)
        };

        this.buttons.register.addEventListener('click', () => this.registerUser());
        this.buttons.login.addEventListener('click', () => this.loginUser());
    }

    appendContentToWrapper(content) {
        this.wrapper.innerHTML = '';
        this.wrapper.innerHTML = content;
    }

    async registerUser() {
        let login = document.querySelector(SELECTORS.USER_LOGIN).value;
        let password = document.querySelector(SELECTORS.USER_PASSWORD).value;

        try {
            let result = await this.client.mutate({
                variables: { email: login, password },
                mutation: queries.registerUser
            });

            if (result.data.register && result.data.register.success) {
                this.showMsg('You are successfuly registered!');
                this.showLoginBlock();
            } else {
                this.showMsg(`Error: can not create user with email: ${login}`);
            }
        } catch (error) {
            this.showMsg(`Error: can not create user with email: ${login}`);
        }
    }

    async loginUser() {
        let login = document.querySelector(SELECTORS.USER_LOGIN).value;
        let password = document.querySelector(SELECTORS.USER_PASSWORD).value;

        try {
            let result = await this.client.mutate({
                variables: { email: login, password },
                mutation: queries.loginUser
            });

            if (result.data) {
                this.showMsg('You are successfuly logged in!');
            } else {
                this.showMsg(`Error: can not login user with email: ${login}`);
            }
        } catch (error) {
            this.showMsg(`Error: can not login user with email: ${login}`);
        }
    }

    showMsg(msg) {
        this.appendContentToWrapper(`<h3>${msg}</h3>`);
    }

    showLoginBlock() {
        let registerTitle = document.querySelector(SELECTORS.REGISTER_TITLE);
        let loginTitle = document.querySelector(SELECTORS.LOGIN_TITLE);

        registerTitle.setAttribute('hidden', 'hidden');
        loginTitle.removeAttribute('hidden');

        this.buttons.register.setAttribute('hidden', 'hidden');
        this.buttons.login.removeAttribute('hidden');
    }
}
