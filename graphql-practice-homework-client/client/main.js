import { ApolloClient, InMemoryCache, HttpLink } from 'apollo-boost';
import Application from './application';

const GRAPHQL_PORT = 3000;
const client = new ApolloClient({
    link: new HttpLink({ uri: `http://localhost:${GRAPHQL_PORT}/graphql` }),
    cache: new InMemoryCache(),
});

window.addEventListener('load', () => {
    const builder = new Application(client);
});
