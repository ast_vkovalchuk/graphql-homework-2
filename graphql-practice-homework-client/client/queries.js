import { gql } from 'apollo-boost';

export const flights = gql`
    query getFlights {
        flights {
            id
            name
        }
    }
`;

export const registerUser = gql`
    fragment ResponseFragment on Response {
        ... on SuccessResponse {
        success
        }
        ... on ErrorResponse {
        message
        }
    }

    mutation registerUser($email: String!, $password: String!) {
        register(credentials: { email: $email, password: $password }) {
            ...ResponseFragment
        }
    }
`;

export const loginUser = gql`
    mutation loginUser ($email: String!, $password: String!) {
        login(credentials: { email: $email, password: $password }) {
            ... on User {
                id
                email
            }
            ... on ErrorResponse {
                message
            }
        }
    }
`;


