import ApolloDSRest from 'apollo-datasource-rest';
const { RESTDataSource } = ApolloDSRest;

export class SpaceXDS extends RESTDataSource {
    constructor() {
      super();
      this.baseURL = 'https://api.spacexdata.com/v3';
    }
  
    async getFlights() {
      return this.get('/launches/upcoming');
    }
}