import Apollo from 'apollo-server-koa';
const { gql } = Apollo;

export const typeDefs = gql`
    type Query {
        flights: [Flight]!
        trips(userId: String!): [Trip]
    }

    type Mutation {
        login(credentials: UserCredentials): UserResponse
        register(credentials: UserCredentials): Response
        bookTrip(userId: String!, tripId: String!): Response
        deleteTrip(userId: String!, tripId: String!): Response
    }

    type User {
        id: String!
        email: String!
    }

    input UserCredentials {
        email: String!
        password: String!
    }
    
    type SuccessResponse {
        success: Boolean!
    }

    type ErrorResponse {
        message: String!
    }

    type Flight {
        id: Int!
        name: String!
    }
    
    type Trip {
        id: String
        user: String
        trip: String
    }

    union Response = SuccessResponse | ErrorResponse
    union UserResponse = User | ErrorResponse
`;