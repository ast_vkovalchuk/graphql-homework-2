import ApolloDSRest from 'apollo-datasource-rest';
const { RESTDataSource } = ApolloDSRest;

export class LocalServiceDS extends RESTDataSource {
    constructor() {
      super();
      this.baseURL = 'http://localhost:4000';
    }

    async registerUser(credentials) {
      return this.post(
          `/users`,
          { email: credentials.email, password: credentials.password }
      );
    }

    async loginUser(credentials) {
      return this.post(
        `/login`,
        { email: credentials.email, password: credentials.password }
      );
    }

    async bookTripForUser(data) {
      return this.post(`/trips`, data);
    }

    async getUserTrips(userId) {
      return this.get(`/trips/${userId}`);
    }

    async deleteTripForUser(userId, tripId) {
      return this.delete(`/trips/${userId}/${tripId}`);
    }
}