export const resolvers = {
    Query: {
        flights: async (_, _args, { dataSources }) => {
            let flights = await dataSources.SpaceXDS.getFlights();

            return flights.map(flight => ({
                id: flight.flight_number,
                name: flight.mission_name
            }));
        },
        trips: async (_, { userId }, { dataSources }) => {
            return await dataSources.LocalServiceDS.getUserTrips(userId);
        }
    },
    Mutation: {
        register: async (_, { credentials }, { dataSources }) => {
            return await dataSources.LocalServiceDS.registerUser(credentials);
        },
        login: async (_, { credentials }, { dataSources }) => {
            return await dataSources.LocalServiceDS.loginUser(credentials);
        },
        bookTrip: async (_, { userId, tripId }, { dataSources }) => {
            return await dataSources.LocalServiceDS.bookTripForUser({id: userId, tripId});
        },
        deleteTrip: async (_, { userId, tripId }, { dataSources }) => {
            return await dataSources.LocalServiceDS.deleteTripForUser(userId, tripId);
        }
    },
    Response: {
        __resolveType(obj, context, info) {
            if (obj.success) {
                return 'SuccessResponse';
            }
            return 'ErrorResponse';
        }
    },
    UserResponse: {
        __resolveType(obj, context, info) {
            if (obj.email) {
                return 'User';
            }
            return 'ErrorResponse';
        }
    }
};