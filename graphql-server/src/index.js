import Apollo from 'apollo-server-koa';
import Koa from 'koa';

const { ApolloServer } = Apollo;

import { typeDefs, resolvers, SpaceXDS, LocalServiceDS } from './booking/index.js';

const PORT = 3000;
const app = new Koa();
const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => {
        return {
            SpaceXDS: new SpaceXDS(),
            LocalServiceDS: new LocalServiceDS()
        };
    }
});

app
    .use(server.getMiddleware())
    .listen({ port: PORT }, () => {
        console.log(`http://localhost:${PORT}${server.graphqlPath}`);
    });