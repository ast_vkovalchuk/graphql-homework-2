import bcrypt from 'bcrypt';
import { promisify } from 'util';

const hash = promisify(bcrypt.hash);
const compare = promisify(bcrypt.compare);
const genSalt = promisify(bcrypt.genSalt);

export default class Encryption {
    static async cryptPassword(password) {
        let encrypted, salt;

        try {
            salt = await genSalt(10);
            encrypted = await hash(password, salt);
        } catch (error) {
            console.log(error);
            return null;
        }

        return encrypted;
    }

    static async comparePassword(plainPass, encrypted) {
        let isPasswordMatch = false;

        try {
            isPasswordMatch = await compare(plainPass, encrypted);
        } catch (error) {
            console.log(error);
        }

        return isPasswordMatch;
    };
}
